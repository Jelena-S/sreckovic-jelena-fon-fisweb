<%-- 
    Document   : menu
    Created on : Apr 22, 2020, 1:42:59 PM
    Author     : Win10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"  crossorigin="anonymous">
    </head>
    <body>

        <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <a class="nav-link" href="/jelena/app/department/save">Add department</a>
            <a class="nav-link" href="/jelena/app/department/all">All departments</a>
            <a class="nav-link" href="/jelena/app/logout">Logout</a>
            <a class="nav-link disabled" href="#">User: ${sessionScope.loginUser.ime}</a>
        </nav>

        <br><br>
    </body>
</html>
