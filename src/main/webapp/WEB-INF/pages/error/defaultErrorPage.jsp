<%-- 
    Document   : defaultErrorPage
    Created on : Apr 21, 2020, 8:31:35 PM
    Author     : Win10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm" align="center">
                    <h1>This is default error page!</h1>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </body>
</html>
