<%-- 
    Document   : AllDepartments
    Created on : Apr 21, 2020, 9:26:09 PM
    Author     : Win10
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>All departments</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm-6" align="center">
                    <h1>Departments:</h1>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Shortname</th>
                                <th scope="col">Name</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="department" items="${applicationScope.departments}">
                                <tr>
                                    <td>${department.id}</td>
                                    <td>${department.shortname}</td>
                                    <td>${department.name}</td>
                                    <td>
                                        <a href="/jelena/app/department/update?id=${department.id}">Edit</a>
                                    </td>
                                    <td>
                                        <a href="/jelena/app/department/delete?id=${department.id}">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach>



                        </tbody>
                    </table>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>

    </body>
</html>
