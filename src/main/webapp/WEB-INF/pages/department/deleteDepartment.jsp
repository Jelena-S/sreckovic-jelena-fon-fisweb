<%-- 
    Document   : deleteDepartment
    Created on : Apr 22, 2020, 12:37:57 PM
    Author     : Win10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete department</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm" align="center">
                    ${message}
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </body>
</html>
