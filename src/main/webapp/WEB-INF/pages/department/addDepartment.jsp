<%-- 
    Document   : addDepartment
    Created on : Apr 21, 2020, 9:35:22 PM
    Author     : Win10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add department</title>
    </head>
    <body>

        <%@include file="../template/menu.jsp" %>

        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm" align="center">
                    <br>
                    ${message}

                    <form action="/jelena/app/department/save" method="POST">
                        <br>
                        Name:
                        <div>
                            <input type="text" name="name" value="" />
                        </div>
                        <br>
                        Shortname:
                        <div>
                            <input type="text" name="shortname" value="" />
                        </div>
                        <br>
                        <input type="submit" value="Save"/>
                    </form>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </body>
</html>
