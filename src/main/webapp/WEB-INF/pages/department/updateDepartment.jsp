<%-- 
    Document   : updateDepartment
    Created on : Apr 22, 2020, 11:00:58 AM
    Author     : Win10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update department</title>

    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm-6" align="center">
                    <form action="/jelena/app/department/update?id=${department.id}" method="POST"> <!--  -->    
                        ${message}
                        <br>
                        Name:
                        <div>
                            <input type="text"  name="name" value="${department.name}" />
                        </div>
                        <br>
                        Shortname:
                        <div>
                            <input type="text"  name="shortname" value="${department.shortname}" />

                        </div>
                        <br>
                        <div>
                            <input type="submit" value="Save"/>
                        </div>
                        <br>
                    </form>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </body>
</html>
