<%-- 
    Document   : login
    Created on : Apr 21, 2020, 7:37:50 PM
    Author     : Win10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LOGIN</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"  crossorigin="anonymous">
    </head>
    <style>
        body {
            background-color: #e3f2fd;
        }
    </style>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                </div>
                <div class="col-sm" align="center">
                    <br>
                    <h1>This is login page!</h1>
                    <br>
                    ${message}
                    <br>
                    <form action="/jelena/app/login" method="post">
                        Email:
                        <div>
                            <input type="text" id="email" name="email"/>
                        </div>
                        <br>
                        Password:
                        <div>
                            <input type="password" id="password" name="password"/>
                        </div>
                        <br>
                        <input type="submit" id="Login" value="Log in"/>
                    </form>
                </div>
                <div class="col-sm">
                </div>
            </div>
        </div>
    </body>
</html>
