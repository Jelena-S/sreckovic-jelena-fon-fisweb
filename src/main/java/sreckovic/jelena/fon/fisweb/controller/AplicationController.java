/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.controller;

import javax.servlet.http.HttpServletRequest;
import sreckovic.jelena.fon.fisweb.action.AbstractAction;
import sreckovic.jelena.fon.fisweb.action.factory.ActionFactory;
import sreckovic.jelena.fon.fisweb.constants.PageConstants;

/**
 *
 * @author Win10
 */
public class AplicationController {
    public String processRequest(String pathInfo, HttpServletRequest request) {
        String nextPage = PageConstants.VIEW_DEFAULT_ERROR;
        
        AbstractAction action = ActionFactory.createActionFactory(pathInfo);
        if (action != null) {
            nextPage = action.execute(request);
        }
        
        return nextPage;
    }
}
