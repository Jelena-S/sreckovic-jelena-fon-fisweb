/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.listener;

import java.util.ArrayList;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import sreckovic.jelena.fon.fisweb.model.Department;
import sreckovic.jelena.fon.fisweb.model.User;

/**
 * Web application lifecycle listener.
 *
 * @author Win10
 */
@WebListener
public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("users", createUsers());
        sce.getServletContext().setAttribute("departments", createDepartments());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    private Object createUsers() {
        return new ArrayList<User>(){
            {
                add(new User("Pera","Peric", "pera@gmail.com", "pera"));
                add(new User("Jelena","Sreckovic", "jelena@gmail.com", "jelena"));
                add(new User("Jovana","Sreckovic", "jovana@gmail.com", "jovana"));
            }
        };
    }

    private Object createDepartments() {
        //return new ArrayList<Department>();
        return new ArrayList<Department>(){
            {
                add(new Department(1,"SILAB", "Laboratorija za softversko inzenjerstvo"));
                add(new Department(2,"AI", "Laboratorija za vestacku inteligenciju"));
                add(new Department(3,"MATH", "Katedra za matematiku"));
            }
        };
    }
}

