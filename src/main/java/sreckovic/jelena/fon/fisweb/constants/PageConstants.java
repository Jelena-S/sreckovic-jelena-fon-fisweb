/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.constants;

/**
 *
 * @author Win10
 */
public interface PageConstants {
    
    public static final String VIEW_LOGIN = "VIEW_LOGIN";
    public static final String VIEW_HOME = "VIEW_HOME";
    public static final String VIEW_ADD_DEPARTMENT = "VIEW_ADD_DEPARTMENT";
    public static final String VIEW_ALL_DEPARTMENTS = "VIEW_ALL_DEPARTMENTS";
    public static final String VIEW_UPDATE_DEPARTMENT = "VIEW_UPDATE_DEPARTMENT";
    public static final String VIEW_DELETE_DEPARTMENT = "VIEW_DELETE_DEPARTMENT";
    public static final String VIEW_LOGOUT = "VIEW_LOGOUT";
    public static final String VIEW_DEFAULT_ERROR = "VIEW_DEFAULT_ERROR";
    
    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_HOME = "/WEB-INF/pages/home.jsp";
    public static final String PAGE_ADD_DEPARTMENT = "/WEB-INF/pages/department/addDepartment.jsp";
    public static final String PAGE_ALL_DEPARTMENTS = "/WEB-INF/pages/department/allDepartments.jsp";
    public static final String PAGE_UPDATE_DEPARTMENT = "/WEB-INF/pages/department/updateDepartment.jsp";
    public static final String PAGE_DELETE_DEPARTMENT = "/WEB-INF/pages/department/deleteDepartment.jsp";
    public static final String PAGE_LOGOUT = "/login.jsp";
    public static final String PAGE_DEFAULT_ERROR = "/WEB-INF/pages/error/defaultErrorPage.jsp";
}
