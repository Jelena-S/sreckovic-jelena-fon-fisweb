/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import sreckovic.jelena.fon.fisweb.action.AbstractAction;
import sreckovic.jelena.fon.fisweb.constants.PageConstants;
import sreckovic.jelena.fon.fisweb.model.Department;

/**
 *
 * @author Win10
 */
public class UpdateDeparmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String shortname = request.getParameter("shortname");

        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");

        for (Department department : departments) {
            if (department.getId() == id) {
                if (!department.getShortname().equals(shortname) && shortname != null && !shortname.isEmpty()) {
                    department.setShortname(shortname);
                }
                if (!department.getName().equals(name) && name != null && !name.isEmpty()) {
                    department.setName(name);
                }
                request.setAttribute("department", department);

                if (name == null || shortname == null || name.isEmpty() || shortname.isEmpty()) {
                    request.setAttribute("message", "Enter your changes!");
                } else {
                    request.setAttribute("message", "Department is updated!");
                }
                break;
            }
        }

        return PageConstants.VIEW_UPDATE_DEPARTMENT;
    }

}
