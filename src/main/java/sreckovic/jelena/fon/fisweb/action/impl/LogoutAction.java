/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sreckovic.jelena.fon.fisweb.action.impl;

import javax.servlet.http.HttpServletRequest;
import sreckovic.jelena.fon.fisweb.action.AbstractAction;
import sreckovic.jelena.fon.fisweb.constants.PageConstants;

/**
 *
 * @author Win10
 */
public class LogoutAction extends AbstractAction{
    @Override
    public String execute(HttpServletRequest request) {
        
        request.getSession(true).setAttribute("loginUser", null);
        
        return PageConstants.VIEW_LOGOUT;
    }

    
}
