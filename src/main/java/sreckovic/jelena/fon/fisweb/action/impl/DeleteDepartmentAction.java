/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import sreckovic.jelena.fon.fisweb.action.AbstractAction;
import sreckovic.jelena.fon.fisweb.constants.PageConstants;
import sreckovic.jelena.fon.fisweb.model.Department;

/**
 *
 * @author Win10
 */
public class DeleteDepartmentAction extends AbstractAction{

    @Override
    public String execute(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));

        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        
        for (Department department : departments) {
            if (department.getId() == id) {
                departments.remove(department);
                request.setAttribute("message", "Department is removed");
                break;
            }
        }

        return PageConstants.VIEW_DELETE_DEPARTMENT;
    }
    
}
