/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import sreckovic.jelena.fon.fisweb.action.AbstractAction;
import sreckovic.jelena.fon.fisweb.constants.PageConstants;
import sreckovic.jelena.fon.fisweb.model.Department;

/**
 *
 * @author Win10
 */
public class AddDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        String name = request.getParameter("name");
        String shortname = request.getParameter("shortname");

        Department department = new Department(shortname, name);
        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        
        if(name == null || shortname == null || name.isEmpty() || shortname.isEmpty()) {
            request.setAttribute("message", "Enter name and shortname of deparment..");
        } 
        else if (departments.contains(department)) {
            request.setAttribute("message", "Department already exist with that name!");
        } else {
            int id = max(departments);
            department.setId(id);
            departments.add(department);
            request.setAttribute("message", "Department is saved!");
        }
        
        return PageConstants.VIEW_ADD_DEPARTMENT;
    }

    private int max(List<Department> departments) {
        int max = -1;
        for (Department department : departments) {
            if (max <= department.getId()) {
                max = department.getId();
            }
        }
        max++;
        return max;
    }

}
