/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sreckovic.jelena.fon.fisweb.action.factory;

import sreckovic.jelena.fon.fisweb.action.AbstractAction;
import sreckovic.jelena.fon.fisweb.action.impl.AddDepartmentAction;
import sreckovic.jelena.fon.fisweb.action.impl.AllDepartmentsAction;
import sreckovic.jelena.fon.fisweb.action.impl.DeleteDepartmentAction;
import sreckovic.jelena.fon.fisweb.action.impl.LoginAction;
import sreckovic.jelena.fon.fisweb.action.impl.LogoutAction;
import sreckovic.jelena.fon.fisweb.action.impl.UpdateDeparmentAction;
import sreckovic.jelena.fon.fisweb.constants.ActionConstants;

/**
 *
 * @author Win10
 */
public class ActionFactory {
    public static AbstractAction createActionFactory(String actionName) {
        AbstractAction action = null;
        if (actionName.equals(ActionConstants.URL_LOGIN)) {
            action = new LoginAction();
        }
        if (actionName.equals(ActionConstants.URL_ALL_DEPARTMENTS)) {
            action = new AllDepartmentsAction();
        }
        if (actionName.equals(ActionConstants.URL_ADD_DEPARTMENT)) {
            action = new AddDepartmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_UPDATE_DEPARTMENT)) {
            action = new UpdateDeparmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_DELETE_DEPARTMENT)) {
            action = new DeleteDepartmentAction();
        }
        if (actionName.equals(ActionConstants.URL_LOGOUT)) {
            action = new LogoutAction();
        }
        
        return action;
    }
}
